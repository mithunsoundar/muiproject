import React from 'react';
import { Grid, Paper, Typography } from '@mui/material';

import SquareBox  from './components/SquareBox'

import Window from './components/Window';

import Door from './components/Door'

const MyGrid = () => {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Paper style={{ padding: '75px', border: '1px solid #000', backgroundColor: 'lightblue', height: '400px', width: '50%', margin: 'auto' }}>
            <Typography variant="h5">House</Typography>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <SquareBox >
                <Window> Window1 </Window>                 
                </SquareBox >
              </Grid>
              <Grid item xs={6} container direction="column" justifyContent="right">
                <Grid item>
                  <SquareBox >
                  <Window> Window2 </Window>
                  </SquareBox >
                </Grid>
                <Grid item style={{ display: 'flex', justifyContent: 'end' }}>
                  <Door>Door</Door>                  
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default MyGrid;
