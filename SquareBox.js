// SquareBox .js
import React from 'react';

const SquareBox  = ({ children }) => {
  return (
    <div style={{ position: 'relative', width: '100%', paddingTop: '100%' }}>
      <div style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>{children}</div>
    </div>
  );
};

export default SquareBox ;
