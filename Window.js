// Window.js
import React from 'react';
import { Paper } from '@mui/material';
import SquareBox from './SquareBox'

const Window = ({ children }) => {
  return (
    <SquareBox>
      <Paper style={{ backgroundColor: '#00ff0080', height: '30%', width: '30%', border: '1px solid #000', textAlign: 'center' }}>
        {children}
      </Paper>
    </SquareBox>
  );
};

export default Window;
