// Door.js
import React from 'react';
import { Paper } from '@mui/material';


const Door = () => {
  return (
    <Paper style={{ backgroundColor: '#00ff0080', height: '439px', width: '180px', border: '1px solid #000', textAlign: 'center', position: 'relative' }}>
      <div style={{ position: 'absolute', top: '50%', transform: 'translateY(-50%)' }}>
        Door
      </div>
    </Paper>
  );
};

export default Door;
